require 'erb'
require 'highline'
h = HighLine.new

core = h.ask('CPU Number: ', Integer)
File.write('conkyrc',ERB.new(DATA.read, nil, ?-).result(binding))
__END__
alignment bottom_right
gap_y 0
gap_x 0
double_buffer yes
own_window yes
own_window_transparent no
own_window_argb_visual yes
own_window_class Conky
own_window_type override
own_window_argb_value 100
stippled_borders 0
update_interval 1.0
uppercase no
use_spacer none
show_graph_scale no
show_graph_range no
use_xft yes
xftfont Kochi Gothic:size=12


TEXT
$nodename
$hr
${color grey}Uptime:$color $uptime
${color grey}RAM Usage:$color $mem/$memmax - $memperc% ${membar 4}
${color grey}Swap Usage:$color $swap/$swapmax - $swapperc% ${swapbar 4}
${color grey}CPU Usage:$color ${cpu cpu0}% ${cpubar cpu0 4}
<%- core.times do |c| -%>
${color grey}    Core<%= c+1 %>($color${freq cpu<%= c + 1 %>}MHz):$color ${cpu cpu<%= c+1 %>}% ${cpubar cpu<%= c+1 %> 4}
<%- end -%>
${color grey}Processes:$color $processes  ${color grey}Running:$color $running_processes
$hr
${color grey}File systems:
/     $color${fs_used /}/${fs_size /} ${fs_bar 6 /}
/home $color${fs_used /home}/${fs_size /home} ${fs_bar 6 /home}
${color grey}Networking:
Up:$color ${upspeed eth0} ${color grey} - Down:$color ${downspeed eth0}
$hr
${color grey}Name              PID   CPU%   MEM%
${color lightgrey} ${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${color lightgrey} ${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${color lightgrey} ${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${color lightgrey} ${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}

