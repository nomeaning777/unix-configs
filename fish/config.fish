function fish_prompt
  echo
  set_color yellow
  echo -n "["$USER"@"(hostname | cut -d . -f 1)"]: "
  set_color --bold cyan
  echo -n (prompt_pwd)" "
  set_color normal
  set_color green
  echo '('(date +"%m/%d %H:%M:%S")')'
  set_color yellow
  echo -n "\$ "
  set_color normal
end

function fish_right_prompt
  set c_status $status
  if [ "$c_status" = "0" ]
    set_color green
    echo "[OK]"
  else
    set_color red
    echo -n "["$c_status"]"
  end
  set_color normal
end

# 環境設定の変数（1回のみ)
# TODO: status --is--loginの利用だとX利用時の挙動がおかしい件について調査
if [ "$FISH_ENV_CONFIGURATED" = "" ] 
  set -x FISH_ENV_CONFIGURATED 1
  set -x PATH $PATH /usr/local/games /usr/games /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /sbin /bin   
  set -x PATH $HOME/bin $HOME/local/work/bin $HOME/local/bin $PATH
  set -x LANG ja_JP.UTF-8
  set -x EDITOR vim
  set -x TEXINPUTS . ~/texmf
  set -x LD_LIBRARY_PATH $HOME/local/lib
  if [ -s $HOME/.rbenv ] 
    set -x PATH $HOME/.rbenv/bin $PATH
  end
  
  #ndenv
  if [ -s $HOME/.ndenv ] 
    set -x PATH $HOME/.ndenv/bin $PATH
    set -x PATH /home/nomeaning/.ndenv/shims $PATH
  end
  # pyenv
  if [ -s $HOME/.pyenv ]
    set -x PYENV_ROOT $HOME/.pyenv
    set -x PATH $PYENV_ROOT/bin $PATH
  end
  # metasmが存在する場合
  [ -s $HOME/local/metasm ]; and set -x RUBYLIB $RUBYLIB $HOME/local/metasm
  
  # ssh-agentの自動起動
  set -l agent_path /tmp/ssh-agent-$USER
  pgrep -u $USER ssh-agent 1> /dev/null
  if [ "$status" = "1" ]
    set -l agent_pid (ssh-agent -a $agent_path | tail -1 | cut -f 4 -d " " | head -c -2)
    set -x SSH_AUTH_SOCK $agent_path
    set -x SSH_AGENT_PID $agent_pid
  else
    set -x SSH_AUTH_SOCK $agent_path
    set -x SSH_AGENT_PID (pgrep -u $USER ssh-agent)
  end

  # nestの場合の環境設定
  if [ (hostname --fqdn) = "nest.mma.club.uec.ac.jp" ]
    set -x HTTP_PROXY http://proxy-east.uec.ac.jp:8080/
    set -x http_proxy $HTTP_PROXY
    set -x HTTPS_PROXY http://proxy-east.uec.ac.jp:8080/
    set -x https_proxy $HTTP_PROXY
    set -x ftp_proxy $HTTP_PROXY
    set -x FTP_PROXY $HTTP_PROXY
    set -x no_proxy 127.0.0.1,localhost,nest,.mma.club.uec.ac.jp
    set -x NO_PROXY $no_proxy

    set -x PATH /opt/vim/bin $PATH
  end
end

if [ -s $HOME/.rbenv ]
  . (rbenv init -|psub)
end

if [ -s $HOME/.pyenv ]
  . (pyenv init -|psub)
end

function fish_greeting

end

# ruby eval
function r
  ruby -e "print $argv"
end

function md5
  echo -n $argv | openssl md5 | cut -f=2 -d " "
end
