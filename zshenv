PROFILE_STARTUP=false
if [[ "$PROFILE_STARTUP" == true ]]; then
  PS4=$'%D{%M%S.} %N:%i> '
  exec 3>&2 2>$HOME/tmp/startlog.$$
  setopt xtrace prompt_subst
fi

\typeset -U PATH # パスの重複を防ぐ

# sbinなどがPATHに存在しない場合は追加する
export PATH=$PATH:/usr/local/games:/usr/games:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ "$HOST" = "combination.local" ]; then
  export PATH=/opt/local/lib/mysql55/bin:$PATH
  export PATH="/opt/local/libexec/gnubin:$PATH"
  export PATH=/opt/local/bin:/opt/local/sbin:$PATH
fi

export PATH=$HOME/bin:$HOME/local/work/bin:$HOME/local/bin:$PATH
export LD_LIBRARY_PATH="$HOME/local/lib"
export TEXINPUTS=.:~/texmf:
export EDITOR=vim

[[ -s "$HOME/.rbenv" ]] && export PATH="$HOME/.rbenv/bin:$PATH"
[[ -s "$HOME/.rbenv" ]] && eval "$(rbenv init -)"

# "nvm use" has a performance issue
[[ -s "$HOME/.nvm" ]] && source ~/.nvm/nvm.sh --no-use
if [[ -s "$NVM_DIR/alias/default" ]] ; then
  export PATH="${NVM_DIR}/versions/node/$(cat $NVM_DIR/alias/default)/bin:${PATH}"
  export NVM_PATH="${NVM_DIR}/versions/node/$(cat $NVM_DIR/alias/default)/lib/node
  export NVM_BIN="${NVM_DIR}/versions/node/$(cat $NVM_DIR/alias/default)/bin
fi
[[ -s "$HOME/.pyenv" ]] && export PYENV_ROOT="$HOME/.pyenv"
[[ -s "$HOME/.pyenv" ]] && export PATH="$PYENV_ROOT/bin:$PATH"
[[ -s "$HOME/.pyenv" ]] && eval "$(pyenv init -)"

# direnv
if which direnv > /dev/null; then
  eval "$(direnv hook zsh)"
fi
# metasm
#[[ -s "$HOME/local/metasm" ]] && export RUBYLIB="$HOME/local/metasm"


if [[ "$PROFILE_STARTUP" == true ]]; then
  unsetopt xtrace
  exec 2>&3 3>&-
fi
